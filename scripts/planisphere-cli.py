#!/usr/bin/env python3
import sys
import argparse
from pyplanisphere.helpers import get_credentials
from pyplanisphere.planisphere import api
import dateparser
import pprint


def parse_args():
    parser = argparse.ArgumentParser(
        description='Script to test queries against the Planisphere API')
    subparsers = parser.add_subparsers(dest='action', help='sub-command help')
    parser_devices = subparsers.add_parser('devices')
    parser_devices.add_argument('-a', '--active_since', type=str,
                                default='14 days ago',
                                help="Active since when?")
    parser_devices.add_argument(
        '--show-support-group-details', action='store_true',
        help="Include information on assigned support group"
    )
    parser_devices.add_argument(
        '--show-user-details', action='store_true',
        help="Include full information on user"
    )
    parser_devices.add_argument(
        '--show-mac-addresses', action='store_true',
        help="Include MAC addresses in output"
    )
    parser_devices.add_argument(
        '-m', '--mac-address', type=str,
        help="Filter by MAC Address"
    )

    subparsers.add_parser('support-groups')
    subparsers.add_parser('departments')

    return parser.parse_args()


def main():
    args = parse_args()
    credentials = get_credentials()
    planisphere = api(server='planisphere.oit.duke.edu',
                      username=credentials['username'],
                      key=credentials['key'])

    if args.action == 'devices':

        active_since = int(dateparser.parse(args.active_since).timestamp())
        params = {
            'active_since': active_since
        }

        if args.show_support_group_details:
            params['support_group_details'] = True
        if args.show_mac_addresses:
            params['show_mac_addresses'] = True
        if args.show_user_details:
            params['user_details'] = True
        if args.mac_address:
            params['mac_address'] = args.mac_address.replace('-', ':')

        for item in planisphere.get_devices(params=params):
            item.pp()
    elif args.action == 'support-groups':
        for item in planisphere.get_support_groups():
            item.pp()
    elif args.action == 'departments':
        for item in planisphere.get_departments():
            for device in item.get_devices():
                print(device)

    return 0


if __name__ == "__main__":
    sys.exit(main())
