import yaml
import os


def get_credentials(*args, **kwargs):
    """
    Pull in credentials from ~/.planisphere.yaml

    Format of file should be

    ```
    ---
    'cartographer.oit.duke.edu':
        username: my_username
        key: my_key
    ```

    Args:

      credentials_file (str): Credential file to use (~/.planisphere.yaml)

      server (str): Server to use (planisphere.oit.duke.edu)

    Returns:

      dict: Values from given credentials file
    """

    credentials_file = kwargs.get('credentials_file', '~/.planisphere.yaml')
    server = kwargs.get('server', 'planisphere.oit.duke.edu')

    config_file = os.path.expanduser(credentials_file)

    with open(config_file, 'r') as stream:
        return yaml.load(stream, Loader=yaml.FullLoader)[server]
