import pprint
import json


class base(object):
    def __init__(self, **kwargs):
        self.raw_data = kwargs
        self.data = self.parse_data()
        return 0

    def parse_data(self):
        return self.raw_data

    def pp(self):
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(self.data)

    def json(self):
        return json.dumps(self.data)

    def __getattr__(self, attr):
        if attr in self.raw_data:
            return self.raw_data[attr]
        else:
            return None
