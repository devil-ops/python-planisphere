from pyplanisphere.objects import base
import dateparser


class Device(base):
    def __init__(self, api, *args, **kwargs):
        self.api = api
        super().__init__(**kwargs)

    def parse_data(self):
        """
        Given the raw json data, return data formatted for python such as:
        * Convert date strings to datetime objects
        * Convert SupportGroups and Departments to objects
        """
        data = {}
        for label, value in self.raw_data.items():
            if label in ['last_active', 'last_in_endpoint_mgmt']:
                value = dateparser.parse(value)
            elif label == 'department_key':
                data['department'] = self.api.get_department(value)
            elif label == 'mac_addresses':
                new_macs = []
                for mac in value:
                    new_macs.append({
                        'address':
                        mac['address'],
                        'last_active':
                        dateparser.parse(mac['last_active'])
                    })
                data['mac_addresses'] = new_macs
            elif label == 'support_group_id':
                data['support_group'] = self.api.get_support_group(value)
            else:
                data[label] = value
        return data

    def quarantine(self, *args, **kwargs):
        """Quarantine a device
        :param reason_type: Can be 'endpoint_management' or 'other'
        :type reason_type: str
        :param reason_notes: Notes for reason
        :param reason_notes: str
        :return: Boolean on if the quarantine succeeds
        :rtype: Boolean
        """

        reason_type = kwargs.get('reason_type', 'other')
        reason_notes = kwargs.get('reason_notes', 'Quarantined using API')

        data = {'type': reason_type, 'notes': reason_notes}

        self.api.query('devices/%s/quarantine_reasons' % self.data['id'],
                       data=data,
                       verb='post')
        return True

    def get_support_group(self, *args, **kwargs):
        """
        :returns: Support group for a given Device
        :rtype: SupportGroup
        """
        if 'support_group' in self.data:
            if not isinstance(self.data['support_group'], dict):
                return self.data['support_group']
            else:
                support_group = self.api.get_support_group(
                    self.data['support_group']['id'])
                return support_group
        else:
            return None

    def get_endpoint_management_status(self, *args, **kwargs):
        """
        :returns: Boolean on if the device is compliant
        :rtype: Boolean
        """

        if self.data['endpoint_mgmt_status'] == 'out_of_compliance':
            return False
        else:
            return True

    def __repr__(self):
        return u"<Device.%s>" % self.data['id']


class SupportGroup(base):
    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.data = kwargs
        self.raw_data = kwargs

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        return u"<SupportGroup.%s>" % self.data['key']


class Department(base):
    def __init__(self, api, *args, **kwargs):
        self.data = kwargs
        self.api = api
        super().__init__(**kwargs)

    def get_devices(self, **kwargs):
        items = []
        params = {'department_details': True}

        for item in self.api.query("devices", params=params):
            if 'department_key' not in item:
                continue
            if item['department_key'] == self.data['key']:
                items.append(item)
        return items

    def __repr__(self):
        return u"<Department.%s>" % self.data['key']
