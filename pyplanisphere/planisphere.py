import requests
import sys
from pyplanisphere.objects.objects import SupportGroup, Department, Device


class api(object):
    """
    Top level pyplanisphere object

    This represents the base API connection
    """

    def __init__(self, *args, **kwargs):

        self.auth = (kwargs['username'], kwargs['key'])
        self.api_base = 'https://%s/api/v1' % kwargs['server']

    def query(self, *args, **kwargs):
        """
        Args:

          verb (str): Verb to query with ('GET')

          params (dict): Parameters to pass to query ({})

          url (str):  URL Path to query

          data (dict): Data to send up (None)

        Returns:

          dict: Result from json
        """

        verb = kwargs.get('verb', 'get')
        params = kwargs.get('params', {})
        data = kwargs.get('data', None)
        url = "%s/%s" % (self.api_base, args[0])

        result = requests.request(verb, url, auth=self.auth, params=params,
                                  data=data)

        #import curlify
        # print(curlify.to_curl(result.request))
        if result.status_code not in [200]:
            sys.stderr.write("%s\n" % result.text)
            raise Exception("Bad Request")

        # Some endpoints don't return json
        try:
            return result.json()
        except Exception as e:
            return result.text

    def get_support_group(self, support_group_id):
        for item in self.get_support_groups():
            if item.id == support_group_id:
                return item
        sys.stderr.write(
            "Could not find support group: %s\n" % support_group_id)
        raise Exception("SupportGroupNotFound")

    def get_support_groups(self):
        return_list = []
        for item in self.query('support_groups'):
            sg = SupportGroup(**item)
            return_list.append(sg)
        return return_list

    def get_quarantine_risks(self, reasons=['endpoint_management',
                                            'unsupported_os',
                                            'vulnerability']):
        return_items = {}
        for reason in reasons:
            return_items[reason] = []
            for item in self.query('devices/report', params={
                'active_since': 7,
                'qurantine_risks[]': reason
            }
            ):
                return_items[reason].append(item)
        return return_items

    def get_department(self, department_key):
        for department in self.get_departments():
            if department.key == department_key:
                return department
        sys.stderr.write("Could not find department: %s\n" % department_key)
        raise Exception("DepartmentNotFound")

    def get_departments(self):
        return_list = []
        for item in self.query('departments'):
            ob = Department(api=self, **item)
            return_list.append(ob)
        return return_list

    def get_devices(self, *args, **kwargs):
        return_list = []
        params = kwargs.get('params', {})
        for item in self.query('devices', params=params):
            ob = Device(api=self, **item)
            return_list.append(ob)
        return return_list
