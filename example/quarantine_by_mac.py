#!/usr/bin/env python3

import sys
import dateparser
from pyplanisphere.helpers import get_credentials
from pyplanisphere.planisphere import api


def main():

    if len(sys.argv) == 1:
        sys.stderr.write("Usage: %s MAC [MAC...]\n" % sys.argv[0])
        return 1

    credentials = get_credentials()
    planisphere = api(server='planisphere.oit.duke.edu',
                      username=credentials['username'],
                      key=credentials['key'])
    active_since = int(dateparser.parse('60 days ago').timestamp())
    params = {
        'active_since': active_since
    }

    for mac_address in sys.argv[1:]:
        params = {
            'support_group_details': True,
            'mac_address': mac_address.replace('-', ':')
        }

        for item in planisphere.get_devices(params=params):
            print("Quarantining %s" % item)
            item.quarantine(reason_type='endpoint_management')
    return 0


if __name__ == "__main__":
    sys.exit(main())
