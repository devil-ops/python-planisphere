#!/usr/bin/env python3

import sys
import dateparser
from pyplanisphere.helpers import get_credentials
from pyplanisphere.planisphere import api
from datetime import datetime, timedelta


def main():

    if len(sys.argv) == 1:
        sys.stderr.write("Usage: %s MAC [MAC...]\n" % sys.argv[0])
        return 1

    credentials = get_credentials()
    planisphere = api(server='planisphere.oit.duke.edu',
                      username=credentials['username'],
                      key=credentials['key'])
    active_since = int(dateparser.parse('30 days ago').timestamp())

    for mac_address in sys.argv[1:]:
        params = {
            'mac_address': mac_address.replace('-', ':'),
            'show_endpoint_mgmt_info': True,
            'active_since': active_since
        }

        for item in planisphere.get_devices(params=params):
            print("Info status for %s is %s" % (item, item.raw_data))
    return 0


if __name__ == "__main__":
    sys.exit(main())
