#!/usr/bin/env python3

import sys
from pyplanisphere.helpers import get_credentials
from pyplanisphere.planisphere import api


def main():

    credentials = get_credentials()
    planisphere = api(server='planisphere.oit.duke.edu',
                      username=credentials['username'],
                      key=credentials['key'])

    for risk_type, items in planisphere.get_quarantine_risks().items():
        for item in items:
            print(risk_type, item)
    return 0


if __name__ == "__main__":
    sys.exit(main())
