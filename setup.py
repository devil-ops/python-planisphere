import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as requirements_file:
    install_requirements = requirements_file.read().splitlines()

setup(
    name="python-planisphere",
    version="0.0.17",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Python API wrapper for Planisphere"),
    license="MIT",
    keywords="planisphere",
    packages=find_packages(),
    install_requires=install_requirements,
    scripts=['scripts/planisphere-cli.py'],
    long_description=read('README.md'),
)
