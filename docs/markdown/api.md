<h1 id="pyplanisphere.planisphere.api">api</h1>

```python
api(self, *args, **kwargs)
```

Top level pyplanisphere object

This represents the base API connection

<h2 id="pyplanisphere.planisphere.api.query">query</h2>

```python
api.query(self, *args, **kwargs)
```

Args:

  verb (str): Verb to query with ('GET')

  params (dict): Parameters to pass to query ({})

  url (str):  URL Path to query

Returns:

  dict: Result from json

