<h1 id="pyplanisphere.helpers">pyplanisphere.helpers</h1>


<h2 id="pyplanisphere.helpers.get_credentials">get_credentials</h2>

```python
get_credentials(*args, **kwargs)
```

Pull in credentials from ~/.planisphere.yaml

Format of file should be

```
---
'cartographer.oit.duke.edu':
    username: my_username
    key: my_key
```

Args:

  credentials_file (str): Credential file to use (~/.planisphere.yaml)

  server (str): Server to use (cartographer.oit.duke.edu)

Returns:

  dict: Values from given credentials file

