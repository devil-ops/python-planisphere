# Description

Python wrapper for the Planisphere API

# Setup

Make sure you have a ~/.plansiphere.yaml file that contains something like:

```
---
'cartographer.oit.duke.edu':
  username: your_netid
  key: your_key
```

You can get a key from here [https://planisphere.oit.duke.edu/api_info](https://planisphere.oit.duke.edu/api_info)


## Installation using PIP

```bash
pip install git+https://gitlab.oit.duke.edu/devil-ops/python-planisphere.git#egg=python-planisphere --trusted-host pypi.org
```
